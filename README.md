# AliExpress Posting Bot #

Send an AliExpress goods link and this bot will create a telegram post with a picture, text, cost etc. <br>
Post will be sent to the Telegram channel that selling some goods from AliExpress.

### How do I get set up? ###

* `cp .env.example .env`
* Create all the accounts and save API keys into `.env`
* `docker-compose up -d nginx mysql workspace`
* Check out `routes` directory and start hacking APIs
