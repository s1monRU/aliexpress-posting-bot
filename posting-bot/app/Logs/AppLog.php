<?php


namespace App\Logs;


use Illuminate\Support\Facades\Log;

class AppLog extends Log
{
    protected static string $channel;

    public function __construct()
    {
        self::$channel = env('APP_LOG_CHANNEL', 'daily');
    }

    protected static function log(string $level, string $message, array $contextual = [])
    {
        $channel = env('APP_LOG_CHANNEL', 'daily');
        parent::channel($channel)->{$level}($message, $contextual);
    }

    public static function emergency(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function alert(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function critical(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function error(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function warning(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function notice(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function info(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }

    public static function debug(string $message, array $contextual = [])
    {
        self::log(__FUNCTION__, $message, $contextual);
    }
}
