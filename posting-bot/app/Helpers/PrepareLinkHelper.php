<?php


namespace App\Helpers;


use App\Services\UrlShortenerService\UrlShortener;
use Illuminate\Support\Str;

class PrepareLinkHelper
{
    public string $link;

    public function __construct(string $link)
    {
        $this->link = $link;
    }

    public function removeParameters()
    {
        $this->link = Str::before($this->link, '?');
        return $this;
    }

    public function createDeepLink()
    {
        $this->link = env('BASE_DEEPLINK') . urlencode($this->link);
        return $this;
    }

    public function createShortLink()
    {
        $shortener = new UrlShortener;
        $this->link = $shortener->shorten($this->link);
        return $this;
    }
}
