<?php


namespace App\Services\UrlShortenerService;


use Illuminate\Support\Facades\Http;

class UrlShortener
{
    protected string $apiUrl = 'https://cutt.ly/api/api.php';

    public function shorten(string $link): string
    {
        $response = Http::get($this->apiUrl, [
            'key' => env('LINK_SHORTENER_API_KEY'),
            'short' => $link
        ]);

        $body = json_decode($response->body());
        return $body->url->shortLink;
    }
}
