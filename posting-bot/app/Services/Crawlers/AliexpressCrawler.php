<?php

namespace App\Services\Crawlers;

use \Illuminate\Support\Facades\Http;
use Symfony\Component\DomCrawler\Crawler;

abstract class AliexpressCrawler
{
    public string $url;
    protected string $html;
    protected Crawler $crawler;

    public function __construct(string $url)
    {
        $this->url = $url;
        $this->load();
        $this->crawler = new Crawler($this->html);
    }

    private function load(): void
    {
        $this->html = Http::get($this->url);
    }
}
