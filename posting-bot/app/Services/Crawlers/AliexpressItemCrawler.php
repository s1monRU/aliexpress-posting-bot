<?php

namespace App\Services\Crawlers;

use Illuminate\Support\Str;

class AliexpressItemCrawler extends AliexpressCrawler
{
    public function __construct(string $url)
    {
        parent::__construct($url);
    }

    public function getPrice(): string
    {
        $metaTagTitleRawData = $this->crawler->filter('meta[property="og:title"]')->attr('content');
        return Str::before($metaTagTitleRawData, '. ');
    }

    public function getImageLink(): string
    {
        return $this->crawler->filter('meta[property="og:image"]')->attr('content');
    }


}
