<?php


namespace App\Services\BotClientService;


use App\Logs\AppLog;
use App\Models\Item;

class BotSender extends BotConnector
{
    public function sendPhoto()
    {
        $item = $this->collectPostData();

        if (!$item) {
            return null;
        }

        $costPhrase = $this->costPhraseGenerator();

        $item->sent = 1;
        $item->save();

        $response = $this->client->post('sendPhoto', [
            'chat_id' => $this->chatId,
            'photo' => $item->image_link,
            'caption' => "$costPhrase $item->cost. \n$item->caption \n$item->link"
        ])->throw(function ($response, $e) use ($item) {
            AppLog::warning('BotSender. Request failed', ['ItemId' => $item->id, 'TelegramDescription' => $response->json()['description']]);
        });

        AppLog::info('Item has been sent successfully', ['ItemId' => $item->id]);
    }

    private function collectPostData(): ?Item
    {
        return Item::where('sent', 0)
            ->orderBy('priority')
            ->orderBy('created_at')
            ->first();
    }

    public function costPhraseGenerator(): string
    {
        $phrases = [
            'Стоит всего', 'Сколько денег:', 'Цена:', 'Цена вопроса:',
            'По бабосикам', 'По бабкам выйдет', 'Обойдется в'
        ];
        return $phrases[array_rand($phrases)];
    }
}
