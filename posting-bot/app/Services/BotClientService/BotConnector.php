<?php


namespace App\Services\BotClientService;


use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

abstract class BotConnector
{
    public $chatId;
    protected $client;

    public function __construct()
    {
        $this->chatId = App::environment('production') ? env('CHANNEL_NAME') : env('TEST_CHANNEL_NAME');
        $baseLink = 'https://api.telegram.org/bot' . env('BOT_KEY');

        $this->client = Http::baseUrl($baseLink);
    }
}
