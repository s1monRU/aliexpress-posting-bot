<?php

namespace App\Http\Controllers;

use App\Helpers\PrepareLinkHelper;
use App\Http\Requests\CreateItem;
use App\Models\Item;
use App\Services\Crawlers\AliexpressItemCrawler;

class ItemsController extends Controller
{
    public function create(CreateItem $request)
    {
        $prepareLinkHelper = new PrepareLinkHelper($request->link);
        $shortlink = $prepareLinkHelper->removeParameters()->createDeepLink()->createShortLink()->link;
        $crawler = new AliexpressItemCrawler($request->link);

        $item = new Item;
        $item->caption = $request->caption;
        $item->priority = $request->priority;
        $item->link = $shortlink;
        $item->image_link = $crawler->getImageLink();
        $item->cost = $crawler->getPrice();
        $item->save();

        return response()->json($item->getAttributes());
    }
}
