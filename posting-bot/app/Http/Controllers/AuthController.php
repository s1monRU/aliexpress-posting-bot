<?php

namespace App\Http\Controllers;

use App\Logs\AppLog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $attributes = $request->validate([
            'email' => 'email|string|email',
            'password' => 'required|string|min:6'
        ]);

        if (!Auth::attempt($attributes)) {
            AppLog::notice('Auth. Failed attempt to login', $attributes);
            return response()->json('Wrong credentials', 401);
        }

        return response()->json([
            'token' => auth()->user()->createToken('API Token')->plainTextToken
        ]);
    }
}
